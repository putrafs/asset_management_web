<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['middleware' =>'guest', function(){
  return view('welcome');
}]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/microsoft', function(){
	return redirect('login/microsoft');
})->name('microsoft');

Route::group(['prefix' => 'tipe/'], function(){
	Route::get('/', 'tipeController@index')->name('tipe');
	Route::get('/ajax', 'tipeController@ajax');
});
Route::group(['prefix' => 'penempatan/'], function(){
	Route::get('/', 'penempatan@index')->name('penempatan');
});
Route::group(['prefix' => 'asset/'], function(){
	Route::get('/', 'asset@index')->name('asset');
});

