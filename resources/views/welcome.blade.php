<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 54px;
            }

            .links > a {
                color: #FFE535;
                padding: 5px 20px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                background-color: #1C80BF;
                transition-duration: 0.3s;
            }

            .links > a:hover{
                background-color: #FFE535;
                color: #1C80BF;
            }

            .links{
                margin-top: 30px;
            }

            .logo{
                width: 20%;
            }

            .copyright{
                position: absolute;
                bottom: 10px;
                font-size: 10px;
            }

            .copyright > b {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <img class="logo" src="{{url('')}}/img/people.png">
                <div class="title">Asset Management</div>
                <div>PT Waskita Toll Road</div>
                <div class="links">
                    <a href="{{url('')}}/login/microsoft">Login</a>
                </div>
            </div>
            <div class="copyright"><b>Created by </b>Shafly Naufal Adianto</div>
        </div>
    </body>
</html>
