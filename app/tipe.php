<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipe extends Model
{
    protected $table = 'tipe';
    protected $fillable = ['nama'];
}
