<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
        
    }

    public function list(){
        $token = $this->getToken();
        $user = $this->getUser($token, NULL);

        if(isset($user->{"@odata.nextLink"})){
            $cek = $user->{"@odata.nextLink"};
        }

        $hasil = $user->value;

        while(1){
            if(isset($cek)){
                $user = $this->getUser($token, $cek);

                if(isset($user->{"@odata.nextLink"})){
                    $cek = $user->{"@odata.nextLink"};
                }else{
                    $cek = NULL;
                }

                $hasil = array_merge($hasil, $user->value);
            }else break;
        }

        dd($hasil);
    }

    public function getToken(){
        $client = new Client();
        $res = $client->request('POST', 'https://login.microsoftonline.com/524d92b4-8447-4afc-9e67-b47267af7e69/oauth2/token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => 'f170db2b-a0bd-44ad-8cd7-badc2e009641',
                'client_secret' => 'xL?318xb4.cMl@s@FwF5G.aZ/04qAk?p',
                'resource' => 'https://graph.microsoft.com'
            ]
        ]);

        $result= $res->getBody()->getContents(); 
        $hasil = json_decode($result);
        
        return $hasil->access_token;
    }

    public function getUser($token, $url){
        $client = new Client();

        if(!isset($url)){
            $url = 'https://graph.microsoft.com/v1.0/users';
        }

        $res = $client->request('GET', $url, [
            'headers' => [
                'Authorization' => 'Bearer '.$token,
            ],
        ]);

        $result= $res->getBody()->getContents(); 
        $hasil = json_decode($result);

        return $hasil;
    }
}
