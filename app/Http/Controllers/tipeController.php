<?php

namespace App\Http\Controllers;

use App\tipe;
use Illuminate\Http\Request;

class tipeController extends Controller
{
    public function index()
    {
        return view('tipe');
        
    }

    public function ajax(){
    	return datatables(tipe::all())->toJson();
    }
}
