@extends('layouts.app')

@section('style')
<link rel="stylesheet" href="{{url('/')}}/plugin/datatables/dataTables.bootstrap.min.css" />
<link rel="stylesheet" href="{{url('/')}}/plugin/datatables/responsive.dataTables.min.css" />
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Data Tipe
                </div>
                <div class="card-body">
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nama</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- datatbles -->
<script src="{{url('')}}/plugin/jQuery/jQuery-2.1.4.min.js"></script>
<script type="text/javascript" src="{{url('')}}/plugin/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{url('')}}/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="{{url('')}}/plugin/datatables/dataTables.responsive.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
          responsive: true,
          "aaSorting": [],
          processing: true,
          serverSide: true,
          ajax: '{{url('')}}/tipe/ajax',
          columns: [
          { data: 'nama', name: 'nama' }
          ]
      });
    });
</script>
@endsection
