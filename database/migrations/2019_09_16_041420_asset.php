<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Asset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal_beli');
            $table->text('deskripsi');
            $table->bigInteger('id_tipe')->unsigned();
            $table->string('barcode', 30);
            $table->bigInteger('id_penempatan')->unsigned();
            $table->bigInteger('nilai');
            $table->string('kondisi_barang', 5);
            $table->string('pic', 30);
            $table->string('jenis_barang', 5);
            $table->string('serial_number', 30)->nullable()->default(NULL);
            $table->string('mac_address', 30)->nullable()->default(NULL);
            $table->timestamps();

            $table->foreign('id_tipe')->references('id')->on('tipe');
            $table->foreign('id_penempatan')->references('id')->on('penempatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aset');
    }
}
